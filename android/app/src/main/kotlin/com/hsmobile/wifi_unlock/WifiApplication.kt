package com.hsmobile.wifi_unlock

import android.content.Context
import android.net.wifi.ScanResult
import android.net.wifi.WifiManager

class WifiApplication constructor(context: Context){

    val context:Context
    val wifiManager:WifiManager

    init {
        this.context = context
        this.wifiManager = context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
    }

    fun getWifiAvailable(): String{
        val availNetworks:List<ScanResult> = wifiManager.getScanResults()
        val wifis:MutableSet<String> = mutableSetOf<String>()
        if(availNetworks.size > 0){
            for (item in availNetworks){
                wifis.add(item.toString())
            }
        }
        return wifis.toString()
    }
}