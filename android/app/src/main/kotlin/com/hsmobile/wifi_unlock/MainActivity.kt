package com.hsmobile.wifi_unlock

import io.flutter.embedding.android.FlutterActivity
import android.content.Intent
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel

//Enable this line if use zalo login
//import com.zing.zalo.zalosdk.oauth.ZaloSDK


class MainActivity : FlutterActivity() {
//    Enable this line if use zalo login
//    override fun onActivityResult(requestCode:Int, resultCode:Int, data:Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        ZaloSDK.Instance.onActivityResult(this, requestCode, resultCode, data)
//    }
    private val WIFI_CHANNEL = "/wifi_channel"
    private lateinit var channel: MethodChannel

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        channel = MethodChannel(flutterEngine.dartExecutor.binaryMessenger, WIFI_CHANNEL)
        channel.setMethodCallHandler{call, result ->
            if(call.method == "getWifiAvailable"){
                val r = WifiApplication(context).getWifiAvailable()
                result.success(2)
            }
        }
    }
}

