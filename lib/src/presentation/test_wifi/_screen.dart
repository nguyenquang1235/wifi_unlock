import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class TestWifiScreen extends StatefulWidget {
  const TestWifiScreen({Key? key}) : super(key: key);

  @override
  _TestWifiScreenState createState() => _TestWifiScreenState();
}

class _TestWifiScreenState extends State<TestWifiScreen> {
  late TestWifiViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<TestWifiViewModel>(
        viewModel: TestWifiViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        // child: WidgetBackground(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: SafeArea(child: _buildBody()),
          );
        });
  }

  Widget _buildBody() {
    return SizedBox();
  }
}
