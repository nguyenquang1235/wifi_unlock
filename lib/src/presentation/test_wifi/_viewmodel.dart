import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class TestWifiViewModel extends BaseViewModel {

  final MethodChannel wifiChannel = MethodChannel("/wifi_channel");

  init() async {
    print(await getAvailableWifi());
  }

  Future getAvailableWifi() async {
    return await wifiChannel.invokeMethod("getWifiAvailable");
  }
}
